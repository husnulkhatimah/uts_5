<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class simpan extends Model
{
    protected $table ="simpans";
    protected $primaryKey="id";
    protected $fillable = [
        'id','kode_barang','nama_barang','harga_barang'];
}
