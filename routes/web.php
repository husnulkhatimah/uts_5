<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SimpanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('info', function () {
    return view('info');
});
Route::get('/pemesanan', [SimpanController::class,'index'])->name('pemesanan');
Route::get('/tambah', [SimpanController::class,'create'])->name('tambah');
Route::post('/simpan', [SimpanController::class,'store'])->name('simpan');
Route::get('/edit/{id}', [SimpanController::class,'edit'])->name('edit');
Route::put('/update/{id}', [SimpanController::class,'update'])->name('update');
Route::get('/delete/{id}', [SimpanController::class,'destroy'])->name('delete');
